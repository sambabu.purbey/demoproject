//
//  Marketlist.swift
//  DemoProject
//
//  Created by Laksh Purbey on 10/28/20.
//  Copyright © 2020 Laksh Purbey. All rights reserved.
//

import Foundation

struct Place : Equatable {

    var title: String?
    var price: Double?
    var qty: Double?
    var foodimg: String?
    
    init(attributes: [String: Any]) {
        self.title = attributes["title"] as? String
        self.price = attributes["price"] as? Double
        self.qty = attributes["qty"] as? Double
        self.foodimg = attributes["foodimg"] as? String

    }

}
