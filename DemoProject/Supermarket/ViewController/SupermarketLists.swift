//
//  SupermarketLists.swift
//  DemoProject
//
//  Created by Laksh Purbey on 10/28/20.
//  Copyright © 2020 Laksh Purbey. All rights reserved.
//

import UIKit

class SupermarketLists: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tbl_SuperMarketPrice: UITableView!
    
    // Data model: These strings will be the data for the table view cells
    let food: [String] = ["Bean", "Apple", "Carrot", "Orange", "Potato"]
    
    let food_img: [String] = ["beans", "apple", "carrots", "oranges", "patatos"]
    
    let food_price: [String] = ["0.65", "0.85", "0.90", "1", "5"]
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "cell"
    
    var str_Qty : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register the table view cell class and its reuse id
        
        // (optional) include this line if you want to remove the extra empty cell divider lines
        // self.tbl_SuperMarketPrice.tableFooterView = UIView()
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        tbl_SuperMarketPrice.delegate = self
        tbl_SuperMarketPrice.dataSource = self
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.food.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        
        let cell = tbl_SuperMarketPrice.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ItemListsTableViewCell
        
        // set the text from the data model
        cell.foodname?.text = self.food[indexPath.row]
        
        cell.food_img.image = UIImage(named: self.food_img[indexPath.row])
        
        if str_Qty != nil {
            
        }
        else {
            cell.food_Price.text = self.food_price[indexPath.row]
        }
        
        cell.valuechange_Stepper.tag = indexPath.row
        cell.valuechange_Stepper.addTarget(self, action: #selector(stepperAction(sender:)), for: .valueChanged)
        
        //Buy
        cell.Buy_Now.tag = indexPath.row
        cell.Buy_Now.addTarget(self, action: #selector(BuyAction(sender:)), for: .touchUpInside)
        
        //Cart
        cell.Addtocart.tag = indexPath.row
        cell.Addtocart.addTarget(self, action: #selector(AddtocartAction(sender:)), for: .touchUpInside)
        
        
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    
    @objc func stepperAction(sender: UIStepper) {
        
        let buttonTag = sender.tag
        
        let data1 = Int(sender.value).description
        
        print("data1",data1)
        
        str_Qty = Int("\(data1)")
        
        let path = IndexPath(item: buttonTag, section: 0)
        let cell = tbl_SuperMarketPrice.cellForRow(at: path) as? ItemListsTableViewCell
        
        print("str_Qty",str_Qty!)
        
        
        if str_Qty != nil {
            
            cell?.food_Qty.text = "\(str_Qty!)"
            
            print("food_Price",food_price)
            print("food_Qty",str_Qty)
            
            let price = Double(self.food_price[buttonTag])
            let Qty   = Double(str_Qty!)
            
            
            let str_CalculatePay = price! * Qty
            let sampleValue: Float = Float(str_CalculatePay)
            
            print("str_CalculatePay",sampleValue.clean)
            cell?.food_Price.text = sampleValue.clean
        }
        else {
            
            cell?.food_Qty.text = "1"
            
        }
        
        tbl_SuperMarketPrice.reloadData()
        
    }
    
    //Buy Now
    @objc func BuyAction(sender: Any) {
        
        //by
        let alert = UIAlertController(title: "Alert", message: "Are You Sure want to Buy?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //Add to cart
    @objc func AddtocartAction(sender: Any) {
        
        //by
        let alert = UIAlertController(title: "Alert", message: "Are You Sure want to Add to cart?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
}

extension Float {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

