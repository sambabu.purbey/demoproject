//
//  ItemListsTableViewCell.swift
//  DemoProject
//
//  Created by Laksh Purbey on 10/28/20.
//  Copyright © 2020 Laksh Purbey. All rights reserved.
//

import UIKit

class ItemListsTableViewCell: UITableViewCell {

    @IBOutlet weak var foodname: UILabel!
    @IBOutlet weak var food_img: UIImageView!
    @IBOutlet weak var food_Price: UILabel!
    @IBOutlet weak var food_Qty: UILabel!
    
    @IBOutlet weak var valuechange_Stepper: UIStepper!
    
    @IBOutlet weak var Buy_Now: UIButton!
    @IBOutlet weak var Addtocart: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
